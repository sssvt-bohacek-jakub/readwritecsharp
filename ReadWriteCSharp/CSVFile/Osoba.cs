﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadWriteCSharp.CSVFile
{
    internal class Osoba
    {
        public int index;
        public string? Jmeno;
        public double KontoNaPenezence;
        public bool Zije;
        public DateTime DatumPridani;


        public Osoba(int index, string? jmeno, double kontoNaPenezence, bool zije,DateTime casvyt)
        {
            this.index = index;
            Jmeno = jmeno;
            KontoNaPenezence = kontoNaPenezence;
            DatumPridani = DateTime.Now;
            Zije = zije;
        }
        public Osoba(int index, string? jmeno, double kontoNaPenezence, bool zije)
        {
            this.index = index;
            Jmeno = jmeno;
            KontoNaPenezence = kontoNaPenezence;
            DatumPridani = DateTime.Now;
            Zije = zije;
        }
    }
}
