﻿using JakUdelatVyberVkonzoli;
using ReadWriteCSharp.CSVFile;
using ReadWriteCSharp.TextFileExamples;

namespace ReadWriteCSharp
{
    internal class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Co chcete dělat?");
                switch (Vyber.GetVyber(new string[] { "Write", "Read" }, 3))
                {
                    case 0:
                        Console.Clear();
                        Console.WriteLine("Jaky format?");
                        switch (Vyber.GetVyber(new string[] { "TXT", "CSV", "XML", "JSON" }, 3))
                        {
                            case 0:
                                TextFIleLoader.StartWrite();
                                break;
                            case 1:
                                CSVFileLoader.StartWrite();
                                break;
                            case 2:
                                break;
                            case 3:
                                break;
                        }
                        break;
                    case 1:
                        Console.Clear();
                        Console.WriteLine("Jaky format?");
                        switch (Vyber.GetVyber(new string[] { "TXT", "CSV", "XML", "JSON" }, 3))
                        {
                            case 0:
                                TextFIleLoader.StartRead();
                                break;
                            case 1:
                                CSVFileLoader.StartLoad();
                                break;
                            case 2:
                                break;
                            case 3:
                                break;
                        }
                        break;
                }
            }
        }
    }
}