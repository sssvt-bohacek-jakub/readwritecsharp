﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace ReadWriteCSharp.CSVFile
{
    public static class CSVFileLoader
    {
        public static void StartWrite()
        {

            Console.Write("Zadej cestu: ");
            string? cesta = Console.ReadLine();

            // Zkontrolování zda uživatel nezadal nic
            if (cesta == null)
            {
                return;
            }

            // Zkontrolovanání zda vůbec soubor existuje (Ano jde to dělat přes try, ale tohle je více efektivní)
            if (File.Exists(cesta) == false)
            {
                return;
            }



            Console.WriteLine("Koukni do CSVFileLoader.cs");

            //Vytvoreni Clas ktere budeme zapisovat
            List<Osoba> list = new List<Osoba>();

            list.Add(new Osoba(0, "Jakub", 456.45, true));
            list.Add(new Osoba(1, "Adam", 44, true));
            list.Add(new Osoba(2, "Matej", 89, false));
            list.Add(new Osoba(3, "Kristian", 0, true));
            list.Add(new Osoba(4, "Vojtech", 74892.789, false));

            // Prevedeni clas na string
            List<string> context = new List<string>();

            foreach (Osoba a in list)
            {
                string x = $"{a.index};{a.Jmeno};{a.KontoNaPenezence};{a.Zije};{a.DatumPridani}";
                context.Add(x);
            }

            //zapsani do souboru
            File.WriteAllLines(cesta,context.ToArray());


            Console.ReadLine();
        }
        public static void StartLoad()
        {
            Console.Write("Zadej cestu: ");
            string? cesta = Console.ReadLine();

            // Zkontrolování zda uživatel nezadal nic
            if (cesta == null)
            {
                return;
            }

            // Zkontrolovanání zda vůbec soubor existuje (Ano jde to dělat přes try, ale tohle je více efektivní)
            if (File.Exists(cesta) == false)
            {
                return;
            }

            //Vytvoreni listu
            List<string?> list = new List<string?>();
            List<Osoba> FinalniNactenyList = new List<Osoba>();

            //Cteni
            StreamReader sr;
            sr = new StreamReader(cesta);

            // Loop ve kterým to bude číst dokud nedoujdou řádky
            while (!sr.EndOfStream)
            {
                string? line = sr.ReadLine();

                list.Add(line);
            }

            sr.Close();


            // Prevedeni stringu na clasu

            foreach (string? line in list)
            {
                if (line != null)
                {
                    string?[] radek = line.Split(';');

                    int idOsoby = Convert.ToInt32(radek[0]);
                    string? Jmeno = radek[1];
                    double Konto = Convert.ToDouble(radek[2]);
                    bool zije = radek[3] == "true" ? true : false;

                    DateTime casvyt = Convert.ToDateTime(radek[4]);

                    FinalniNactenyList.Add(new Osoba(idOsoby, Jmeno, Konto, zije,casvyt));

                }
                
            }

            // Vypsani
            foreach (Osoba a in FinalniNactenyList)
            {
                Console.WriteLine($"Index: {a.index} Jmeno: {a.Jmeno} Konto: {a.KontoNaPenezence} zije: {a.Zije} datum:{a.DatumPridani}");
            }
            Console.ReadLine();
        }
    }
}
