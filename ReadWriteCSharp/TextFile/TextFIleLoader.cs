﻿using JakUdelatVyberVkonzoli;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadWriteCSharp.TextFileExamples
{
    public static class TextFIleLoader
    {
        public static void StartRead()
        {

            // získání cesty
            Console.Write("Zadej cestu: ");
            string? cesta = Console.ReadLine();


            // Zkontrolování zda uživatel nezadal nic
            if (cesta == null)
            {
                return;
            }

            // Zkontrolovanání zda vůbec soubor existuje (Ano jde to dělat přes try, ale tohle je více efektivní)
            if (File.Exists(cesta) == false)
            {
                return;
            }

            // Vytvoření listu do kterého budem přidavat text
            List<string?> Text_Co_Byl_Nacten = new List<string?>();


            // Vytvoření čtenáře, který bude číst ze souboru
            StreamReader sr;
            sr = new StreamReader(cesta);

            // Loop ve kterým to bude číst dokud nedoujdou řádky
            while (!sr.EndOfStream)
            {
                string? line = sr.ReadLine();

                Text_Co_Byl_Nacten.Add(line);
            }

            // je potřeba soubor zavřít, který je otevřený přes StreamReader
            sr.Close();

            // Výpis
            foreach (string? a in Text_Co_Byl_Nacten)
            {
                Console.WriteLine(a);
            }

            Console.ReadLine();

        }

        public static void StartWrite()
        {
            // získání cesty
            Console.Write("Zadej cestu: ");
            string? cesta = Console.ReadLine();

            // Zkontrolování zda uživatel nezadal nic
            if (cesta == null)
            {
                return;
            }

            // Zkontrolovanání zda vůbec soubor existuje (Ano jde to dělat přes try, ale tohle je více efektivní)
            if (File.Exists(cesta) == false)
            {
                return;
            }


            // Vytvorim si proměnou, která bude zapisovat do textového souboru
            StreamWriter wr;

            Console.Clear();


            // Zapisovat do textoveho souboru jde pomocí dvou způsobu
            /*
             * Přepsáním celého souboru - StreamWriter wr = new StreamWriter(cesta,false)
             * 
             * Přidání textu do souboru - StreamWriter wr = new StreamWriter(cesta,true)
             * 
             * Přesněji se jedná o "append" argument
             * */
            Console.WriteLine("Vyberte: ");
            switch (Vyber.GetVyber(new string[] { "Přidavat text", "Přepsta a zapsat text" }, 3))
            {
                case 0:
                    wr = new StreamWriter(cesta, true);
                    break;
                case 1:
                    wr = new StreamWriter(cesta, false);
                    break;

                default://Toto ignorujte (jedna se sytaxovou chybu, kdyby to tu nebylo)
                    wr = new StreamWriter(cesta, false);
                    break;
            }

            /* Zápis do souboru pomocí psani do console
             * 
             * Pokud chcete jen zapsat do souboru stačí toto
             * 
             
                foreach (string? k in kontext)
                {
                wr.WriteLine(k);
                }
                wr.Close();

            */

            // Vytvorim si list kde budou hodnoty
            List<string?> kontext = new List<string?>();

            //zapis do listu
            Console.Clear();
            Console.WriteLine("Zadejte co chcete aby se tam napsalo, pro ukoncení \"ESC*\"\n\n ");
            while (true)
            {
                string? ToCoNapsal = Console.ReadLine();

                if (ToCoNapsal != null)
                {

                    if (ToCoNapsal.ToLower() == "esc*") break;
                    kontext.Add(ToCoNapsal);
                }

            }

            // zapis do souboru
            foreach (string? k in kontext)
            {
                wr.WriteLine(k);
            }

            wr.Close();

        }

    }
}
