﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JakUdelatVyberVkonzoli
{
    public static class Vyber
    {
        static void Napis(string CoNapsat, ConsoleColor BarvaPopředí)
        {
            Console.ForegroundColor = BarvaPopředí;
            Console.Write(CoNapsat);
            Console.ResetColor();
        }
        static void Napis(string CoNapsat, ConsoleColor BarvaPopředí,ConsoleColor BarvaPozadí)
        {
            Console.ForegroundColor = BarvaPopředí;
            Console.BackgroundColor = BarvaPozadí;
            Console.Write(CoNapsat);
            Console.ResetColor();
        }
        /// <summary>
        /// Vrátí index vybrané možnost
        /// </summary>
        /// <param name="Moznosti">Dosďte možnosti, které to zobrazí</param>
        /// <param name="OdsazeníOdHlavnihoTextu">Index řádku na kterém má začít vypisovat (Text se po vybraní smaže - přepiše na " ")</param>
        /// <returns></returns>
        static public int GetVyber(string[] Moznosti, int OdsazeníOdHlavnihoTextu)
        {
            int curent = 0;
            while (true)
            {
                Console.SetCursorPosition(0, OdsazeníOdHlavnihoTextu);
                foreach (string a in Moznosti)
                {
                    Console.WriteLine(a);
                }

                Console.SetCursorPosition(0, OdsazeníOdHlavnihoTextu + curent);
                Vyber.Napis(Moznosti[curent], ConsoleColor.Black, ConsoleColor.White);

                switch (Console.ReadKey().Key)
                {
                    case ConsoleKey.DownArrow:
                        if(curent != Moznosti.Length - 1) { curent++; }
                        break;
                    case ConsoleKey.UpArrow:
                        if (curent != 0) { curent--; }
                        break;
                    case ConsoleKey.Enter:
                        Console.ResetColor();
                        Console.SetCursorPosition(0, OdsazeníOdHlavnihoTextu);
                        for (int i = 0; i != Moznosti.Length; i++)
                        {
                            for (int j = 0; j != Moznosti[i].Length; j++)
                            {
                                Console.Write(" ");
                            }
                            Console.WriteLine();
                        }
                        Console.SetCursorPosition(0, OdsazeníOdHlavnihoTextu);
                        return curent;
                }
                
            }
        }

    }
}
